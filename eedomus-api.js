'use strict';

var request = require('request');
const url = require('url');

var EedomusApi = function (options) {
    var self = this;
    self._options = options || {};

    this.test = function (callbackSuccess, callbackError) {

        var action = 'get';
        var fonction = 'auth.test';
        var opt = {
            url: url.resolve(self._options.url, action) + '?action=' + fonction + '&api_user=' + self._options.api.user + '&api_secret=' + self._options.api.secret,
            json: true,
            encoding: 'binary'
        };

        request(opt, function (error, response, data) {
            if (error || response.statusCode != 200 || data.success == 0)
                callbackError(error || data);
            else
                callbackSuccess(data);
        });
    }

    this.periphList = function (callbackSuccess, callbackError) {

        var action = 'get';
        var fonction = 'periph.list';
        var opt = {
            url: url.resolve(self._options.url, action) + '?action=' + fonction + '&api_user=' + self._options.api.user + '&api_secret=' + self._options.api.secret,
            json: true,
            encoding: 'binary'
        };

        request(opt, function (error, response, data) {
            if (error || response.statusCode != 200 || data.success == 0)
                callbackError(error || data);
            else
                callbackSuccess(data);
        });
    }

    this.getPeriphCaract = function (periph_id, callbackSuccess, callbackError) {

        var action = 'get';
        var fonction = 'periph.caract';
        var opt = {
            url: url.resolve(self._options.url, action) + '?action=' + fonction + '&api_user=' + self._options.api.user + '&api_secret=' + self._options.api.secret + '&periph_id=' + periph_id,
            json: true,
            encoding: 'binary'
        };

        request(opt, function (error, response, data) {
            if (error || response.statusCode != 200 || data.success == 0)
                callbackError(error || data);
            else
                callbackSuccess(data.body);
        });
    }

    this.setPeriphValue = function (periph_id, value, callbackSuccess, callbackError) {

        var action = 'set';
        var fonction = 'periph.value';
        var opt = {
            url: url.resolve(self._options.url, action) + '?action=' + fonction + '&api_user=' + self._options.api.user + '&api_secret=' + self._options.api.secret + '&periph_id=' + periph_id + '&value=' + value,
            json: true,
            encoding: 'binary'
        };

        request(opt, function (error, response, data) {
            if (error || response.statusCode != 200 || data.success == 0)
                callbackError(error || data);
            else
                callbackSuccess(data);
        });
    }

    this.setPeriphMacro = function (macro_id, dynamic_value, callbackSuccess, callbackError) {

        var action = 'set';
        var fonction = 'periph.macro';
        var opt = {
            url: url.resolve(self._options.url, action) + '?action=' + fonction + '&api_user=' + self._options.api.user + '&api_secret=' + self._options.api.secret + '&macro=' + macro_id + '&dynamic_value=' + dynamic_value,
            json: true,
            encoding: 'binary'
        };

        request(opt, function (error, response, data) {
            if (error || response.statusCode != 200 || data.success == 0)
                callbackError(error || data);
            else
                callbackSuccess(data);
        });
    }

}

module.exports = EedomusApi;