module.exports = function (RED) {
    "use strict";

    var request = require('request');
    var eedomusApi = require('./eedomus-api.js');
    var util = require("util");
    var settings = RED.settings;

    function EedomusOutNode(config) {
        RED.nodes.createNode(this, config);
        var node = this;
        this.eedomusCred = RED.nodes.getNode(config.eedomusCred);
        var credentials = this.eedomusCred.credentials;

        this.on('input', function (msg) {
            if (msg.hasOwnProperty("payload")) {
                var payload = msg.payload;
                var eeApi = new eedomusApi({ url: credentials.url, api: { user: credentials.api_user, secret: credentials.api_secret } });

                node.status({ shape: "dot", text: payload });
                eeApi.setPeriphValue(config.periph_id, payload, 
                    function (data) { node.status({ fill: "green", shape: "dot", text: payload }); }, 
                    function (err) { node.error(err, msg); node.status({ fill: "red", shape: "dot" }); }
                    );

                node.send(msg);
            }
        });

        RED.httpAdmin.get('/eedomus-periph-list/:id', RED.auth.needsPermission('eedomus.read'), function (req, res) {

            var node_id = req.query.id;

            var credentials = node.eedomusCred.credentials;
            if (!credentials || !credentials.api_user || !credentials.api_secret || !credentials.url)
                return res.status(400).send("credentials not present?");

            var eeApi = new eedomusApi({ url: credentials.url, api: { user: credentials.api_user, secret: credentials.api_secret } });

            eeApi.periphList(function (data) { res.json(data); }, function (err) { res.status(400).send(JSON.stringify(err)); });
        });
    }

    RED.nodes.registerType("eedomus out", EedomusOutNode);

    function EedomusNodeApiNode(config) {
        RED.nodes.createNode(this, config);
    }

    RED.nodes.registerType("eedomus-credentials", EedomusNodeApiNode, {
        credentials: {
            api_user: { type: "password" },
            api_secret: { type: "password" },
            name: { type: "text" },
            url: { type: "text" }
        }
    });

    function EedomusInNode(config) {
        RED.nodes.createNode(this, config);
        var node = this;
        this.eedomusCred = RED.nodes.getNode(config.eedomusCred);
        var credentials = this.eedomusCred.credentials;

        this.on('input', function (msg) {

            this.periph_id = config.periph_id | msg.periph_id;
            this.onlyvalue = msg.onlyvalue | config.onlyvalue | false;

            if (msg.hasOwnProperty("payload")) {
                var eeApi = new eedomusApi({ url: credentials.url, api: { user: credentials.api_user, secret: credentials.api_secret } });

                node.status({ shape: "dot", text: this.periph_id });
                eeApi.getPeriphCaract(this.periph_id, (data) => { 

                        msg.payload = (!!this.onlyvalue) ? data.last_value : data; 
                        node.send(msg); 
                        
                        node.status({ fill: "green", shape: "dot", text: data.last_value_text || data.last_value });
                     }, 
                    (err) => { node.error(err, msg); node.status({ fill: "red", shape: "dot" }); }
                    );
            }
        });

        RED.httpAdmin.get('/eedomus-periph-list/:id', RED.auth.needsPermission('eedomus.read'), function (req, res) {

            var node_id = req.query.id;

            var credentials = node.eedomusCred.credentials;
            if (!credentials || !credentials.api_user || !credentials.api_secret || !credentials.url)
                return res.status(400).send("credentials not present?");

            var eeApi = new eedomusApi({ url: credentials.url, api: { user: credentials.api_user, secret: credentials.api_secret } });

            eeApi.periphList(function (data) { res.json(data); }, function (err) { res.status(400).send(JSON.stringify(err)); });
        });
    }

    RED.nodes.registerType("eedomus in", EedomusInNode);
}